const { user } = require('../models/user');
const UserService = require('../services/userService');

const userModel = Object.assign({}, user);
delete userModel.id;

const isObjectHasSameFields = (first, second) => {
    return Object.keys(first).every((key) => second.hasOwnProperty(key));
}

const createUserValid = (req, res, next) => {
    const requestUserData = req.body;

    try {
        // reject if some field empty
        if (!isObjectHasSameFields(userModel, requestUserData)) {
            return res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        }

        const errors = fieldsValidation(requestUserData);
        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors.join(', ')
            });
        }

        next();
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
};

const updateUserValid = (req, res, next) => {
    try {
        const id = req.params.id;
        const requestUserData = req.body;

        if (!userExists({ id })) {
            return res.status(404).send({
                error: true,
                message: 'User not found'
            });
        }

        const allowedUpdates = Object.keys(userModel);
        const reqUpdates = Object.keys(requestUserData);
        const isValidUpdate = reqUpdates.every((field) => allowedUpdates.includes(field));

        if (!isValidUpdate) {
            return res.status(400).send({
                error: true,
                message: 'Invalid updates'
            });
        }
        const errors = fieldsValidation(requestUserData);
        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors
            });
        }
        next();
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
};

const fieldsValidation = (user) => {
    const errors = [];
    if (user.hasOwnProperty('email')) {
        if (!isGmail(user.email)) {
            errors.push('Please enter a valid email @gmail.com.');
        }
        if (!uniqueEmail(user.email)) {
            errors.push('The email is already registered. Please sign in.');
        }
    }
    if (user.hasOwnProperty('phoneNumber')) {
        if (!isValidPhone(user.phoneNumber)) {
            errors.push('Please enter a valid phone number (+380*********).');
        }
    }
    if (user.hasOwnProperty('firstName')) {
        if (!isValidMinLength(user.firstName)) {
            errors.push('Please enter a correct first name.')
        }
    }
    if (user.hasOwnProperty('lastName')) {
        if (!isValidMinLength(user.lastName)) {
            errors.push('Please enter a correct last name.')
        }
    }
    if (user.hasOwnProperty('password')) {
        if (!isValidMinLength(user.password, 3)) {
            errors.push('Please enter a password of 3 - 128 characters.')
        }
    }
    return errors;
};

const userExists = (id) => {
    return UserService.search(id);
};

const isValidMinLength = (value, minLength = 3) => {
    return value.trim().length >= minLength;
};

const isGmail = (email) => {
    const reg = /^[a-z0-9]((\.|\+)?[a-z0-9]){1,63}@gmail\.com$/i;

    return email.match(reg);
};

const isValidPhone = (phone) => {
    const reg = /^\+380[0-9]{9}$/;

    return phone.match(reg);
};

const uniqueEmail = (email) => {
    return !UserService.search({ email });
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

const responseMiddleware = (req, res, next) => {
    const { data, err } = res;

    if (err) {
        return res.status(400).send({
            error: true,
            message: err.message
        });
    }

    if (data) {
        res.send(data);
    }
    next();
}

exports.responseMiddleware = responseMiddleware;

const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const fighterModel = Object.assign({}, fighter);
delete fighterModel.id;

const isObjectHasSameFields = (first, second) => {
    return Object.keys(first).every((key) => second.hasOwnProperty(key));
}
const createFighterValid = (req, res, next) => {
    const newFighter = req.body;
    newFighter.health = 100;
    try {
        if (!isObjectHasSameFields(fighterModel, newFighter)) {
            return res.status(400).send({
                error: true,
                message: 'All fields must be filled in.'
            });
        }
        const errors = fieldsValidation(req.body);
        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors
            });
        }
        const { name, power, health, defense } = newFighter;
        req.newFighter = { name, power, health, defense };
        next();
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
};

const updateFighterValid = (req, res, next) => {
    try {
        if (!fighterExists({ id: req.params.id })) {
            return res.status(404).send({
                error: true,
                message: 'Fighter not found'
            });
        }
        const allowedUpdates = Object.keys(fighterModel);
        const reqUpdates = Object.keys(req.body);
        const isValidUpdate = reqUpdates.every((field) => allowedUpdates.includes(field));
        if (!isValidUpdate) {
            return res.status(400).send({
                error: true,
                message: 'Invalid updates'
            });
        }
        const errors = fieldsValidation(req.body);
        if (errors.length) {
            return res.status(400).send({
                error: true,
                message: errors
            });
        }
        next();
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
};

const fieldsValidation = (fighter) => {
    const errors = [];
    if (fighter.hasOwnProperty('name')) {
        if (!minLength(fighter.name, 1)) {
            errors.push('Please enter a name of min 1 characters.');
        }
    }
    if (fighter.hasOwnProperty('power')) {
        if (!checkRange(fighter.power, 1, 10)) {
            errors.push('Power value should be 1...10');
        }
    }
    if (fighter.hasOwnProperty('defense')) {
        if (!checkRange(fighter.defense, 1, 10)) {
            errors.push('Defense value should be 1...10');
        }
    }
    return errors;
};

const fighterExists = (id) => {
    return FighterService.search(id);
};

const minLength = (value, validMinLength) => {
    return value.trim().length >= validMinLength;
};

const checkRange = (value, min, max) => {
    if (!isNaN(value / 1)) {
        return !(value < min || value > max);
    }
    return false;
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res) {
    try {
        const users = UserService.getAll();
        res.send(users);
    } catch (error) {
        res.status(404).send({
            error: true,
            message: 'The requested resource could not be found'
        });
    }
})

router.get('/:id', function (req, res) {
    const id = { id: req.params.id };

    try {
        const user = UserService.search(id);
        if (!user) {
            res.status(404).send({
                error: true,
                message: 'User not found'
            });
        }
        res.send(user);
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
})

router.post('/', createUserValid, (req, res) => {
    try {
        res.send(UserService.create(req.body));
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
});

router.put('/:id', updateUserValid, (req, res) => {
    const id = req.params.id;
    const dataToUpdate = req.body;

    try {
        res.send(UserService.update(id, dataToUpdate));
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
});

router.delete('/:id', function (req, res) {
    const id = req.params.id;

    try {
        const user = UserService.delete(id);
        if (!user) {
            res.status(404).send({
                error: true,
                message: 'User not found'
            });
        }
        res.send(user);
    } catch (e) {
        res.status(500).send({
            error: true,
            message: 'Internal server error'
        });
    }
})
module.exports = router;

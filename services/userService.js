const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO REFACTOR TO THROW error ?
    getAll() {
        const users = UserRepository.getAll();

        if (!users) {
            return null
        }

        return users;
    }

    update(id, dataToUpdate) {
        const user = UserRepository.update(id, dataToUpdate);

        if (!user) {
            return null;
        }

        return user;
    }

    delete(id) {
        const user = UserRepository.delete(id);

        if (!user) {
            return null;
        }

        return user;
    }

    create(userData) {
        const user = UserRepository.create(userData);

        if (!user) {
            return null;
        }

        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();
